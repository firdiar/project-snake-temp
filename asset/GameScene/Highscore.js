Game.HighScore = function(game){

}


Game.HighScore.prototype = {
    create:function(){

        let a = this.add.image(0,0,'ResultBG');
        a.height = 1280;
        a.width = 720;


        var panel = this.add.image(720/2,230,'TopScoreJudul');
        panel.anchor.setTo(0.5);
        panel.scale.setTo(0.7);

        panel = this.add.image(720/2 , 0 , 'Top' );
        panel.anchor.setTo(0.5,0);
        panel.scale.setTo(0.67);

        panel = this.add.image(720/2 , 420 , 'BoxPanel' );
        panel.anchor.setTo(0.5,0);
        panel.scale.setTo(0.67 , 0.62);

        for (let i = 0 ; i < 5 ; i ++){
          panel = this.add.image(720/2 , 430+i*169 , 'top_score_bar' );
          panel.anchor.setTo(0.5,0);
          panel.scale.setTo(0.67);

        }

        var mainmenuBtn = this.add.button(55 , 25 , 'Back_button' , ()=>{target = 'BeforePlay';this.state.start('Loading');},this , 1,0,1);
        mainmenuBtn.scale.setTo(0.7);
        mainmenuBtn.anchor.setTo(0.5,0);
        mainmenuBtn.inputEnabled = true;

        for(var i=0 ; i <5 ; i++ ){
          this.add.text(  720/2-250, 460+i*169, (i+1).toString(),{
            font: "55px Arial",
            fill: "#ffffff",
            align: "left",
            stroke: '#ffffff',
            strokeThickness: 1,
            fontWeight:"bold",
          });

            this.add.text(  720/2-150, 462+i*169, localStorage.getItem((i+1).toString()),{
              font: "55px Arial",
              fill: "#ffffff",
              align: "center",
              stroke: '#ffffff',
              strokeThickness: 1,
              fontWeight:"bold",
            });

            this.add.text(  720/2+110 , 470+i*169, localStorage.getItem((i+1).toString()+'-D'),{
              font: "45px Arial",
              fill: "#ffffff",
              align: "right",
              stroke: '#ffffff',
              strokeThickness: 1,
              fontWeight:"bold",
            });
        }




    }

}
