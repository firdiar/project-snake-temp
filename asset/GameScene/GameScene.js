Game.GameScene = function(game){

}

var card = [];

let item = {
  border : null,
  btn : [],
  timeText : null,
  scoreText : null,
  info : null ,
  hint : null,
  infoReady :null,
  btnPause : null,
  btnResume:null,
  btnRetry : null,
  btnMain : null
};

var tutorialMode = {
  isTutorial : false,
  time : 0,
  panel : null,
  text : null
};

let gameData;

Game.GameScene.prototype = {

    create : function(){

      //membuat setting waktu
      gameData = this.time.create(false);
      gameData.loop(1000 , this.timerUpdate , this);
      gameData.start();

      //membuat setting game
      gameData.maxTime = 124;
      gameData.score = 0;
      gameData.combo = 1;
      gameData.comboTime = 0;
      gameData.level = 1;
      gameData.correct = 0;
      gameData.false = 0;

      this.isPaused = false;

      //munculkan background
      let bg =this.add.image(0,0,'GameBackground');
      bg.height = 1280;
      bg.width = 720;

      let panel3 = this.add.image(720/2 , 0 , 'Top' );
      panel3.anchor.setTo(0.5,0);
      panel3.scale.setTo(0.67);

      item.panelPause = this.add.image(720/2 , 1280/2 , "pause_panel");
      item.panelPause.anchor.setTo(0.5);
      item.panelPause.scale.setTo(0.65);
      item.panelPause.alpha = 0;

      item.btnPause = this.add.button(55 , 25 , 'pause_button' , () => {this.gamePause()} , this , 1 , 0 ,1 );
      item.btnPause.anchor.setTo(0.5,0);
      item.btnPause.scale.setTo(0.7);
      item.btnPause.inputEnabled = true;

      //jika ini adalah tutorial Mode


      item.border = this.add.group();



      let temp = this.add.sprite(720/2,220,'BrushUp',0);
      temp.height = 1280;
      temp.width = 720;
      temp.anchor.setTo(0.5 , 0);
      temp.scale.setTo(0.7);
      temp.animations.add('cleaning');
      temp.play('cleaning' , 10 , true);


      for(let i = 0 ; i < 3 ; i++){
          item.btn[i] = null;
          item.btn[i] = this.add.button(720/2 +225 +(i*-225), 1125 , 'Button',null, this, (4-i*2)+1, 4-i*2, (4-i*2)+1 );
          item.btn[i].inputEnabled = false;
          item.btn[i].anchor.setTo(0.5);
          item.btn[i].scale.setTo(0.74);

          if(i<=1){
            let temp = this.add.sprite(i*720,220,'Brush',0);
            temp.height = 1280;
            temp.width = 720;
            temp.anchor.setTo(0.5 , 0);
            temp.scale.setTo(0.7);
            temp.animations.add('cleaning');
            temp.play('cleaning' , 10 , true);
            item.border.add(temp);
          }
      }

      if(tutorialMode.isTutorial){
          //item.hint.setText('');
          item.btnPause.inputEnabled = false;
          tutorialMode.panel = this.add.image(720/2 , 255 , 'TutorialPanel');
          tutorialMode.panel.anchor.setTo(0.5);
          tutorialMode.panel.height = 300;
          tutorialMode.panel.width = 500;
          tutorialMode.text = this.add.text(  720/2 , 320 , 'Ingat-Ingat Mobil\nSaat Ini' ,{
            font: "30px Arial",
            fill: "#f7f7f7",
            align: "center",
          });
          tutorialMode.text.anchor.setTo(0.5);
      }
      item.hintbg = this.add.sprite(-10,765,'notifbg',0);
      item.hintbg.scale.setTo(1.1,0.65);


      item.hint = this.add.text(  720/2 ,  440 , '3' ,{
        font: "80px Arial",
        fill: "#f7f7f7",
        align: "center",
        fontWeight:"bold"
      });
      item.hint.anchor.setTo(0.5);

      let a = this.add.image(720/2 , 877 , 'Info' , 0);
      a.anchor.setTo(0.5);
      a.scale.setTo(0.7 , 0.6);





      item.layer = this.add.image(0,0 , "LayerBackground");
      item.layer.alpha = 0;


      //item.btnPause.events.onInputDown.add( , this);

      item.btnResume = this.add.button(720/2 , 450, 'Resume_button' , () => {this.gamePause()} , this , 1 , 0 ,1 );
      item.btnResume.anchor.setTo(0.5,0);
      item.btnResume.inputEnabled = false;
      item.btnResume.scale.setTo(0.7);
      item.btnResume.alpha = 0;

      item.btnRetry = this.add.button(720/2 , 650, 'Retry_button' , () => {this.state.start('GameScene')} , this , 1 , 0 ,1 );
      item.btnRetry.anchor.setTo(0.5,0);
      item.btnRetry.inputEnabled = false;
      item.btnRetry.scale.setTo(0.7);
      item.btnRetry.alpha = 0;

      item.btnMain = this.add.button(720/2 , 850, 'MainMenu_button' , () => {target = 'BeforePlay'; this.state.start('Loading')} , this , 1 , 0 ,1 );
      item.btnMain.anchor.setTo(0.5,0);
      item.btnMain.inputEnabled = false;
      item.btnMain.scale.setTo(0.7);
      item.btnMain.alpha = 0;



      let panel = this.add.image(720/2, 0, 'score_panel' );
      panel.anchor.setTo(0.5,0);
      panel.scale.setTo(0.7);
      let panel2 = this.add.image(720/2 +250, 10 , 'timer_panel' );
      panel2.anchor.setTo(0.5,0);
      panel2.scale.setTo(0.7);

      item.timeText = this.add.text( 720/2+225 ,   20 , '02:00' ,{
        font: "30px",
        fill: "#F5F5F5",
        align: "center",
        fontWeight:"bold"
      });
      item.timeText.font = 'Arial';



      item.scoreText = this.add.text(720/2  ,  50 , '0' ,{
        font: "65px",
        fill: "#F5F5F5",
        align: "center",
        fontWeight:"bold"
      });
      item.scoreText.font = 'Arial';
      item.scoreText.anchor.setTo(0.5);


      //card 0 adalah current card
      //card 1 adalah pembanding kartu 0
      card[0] = this.generateNewCard();

      card[1] = null;





    },
    update:function(){
        let timeElapsed = this.time.elapsed/1000;

        if(gameData.comboTime < 1.5){
          gameData.comboTime += timeElapsed;
        }else{
          gameData.combo = 1;
          gameData.comboTime = 0;
        }


    },
    flipCard:function(){


        card[1] = card[0].image.frame;

        card[0].move(-200 , this , true);


        card[0] = this.generateNewCard();



    },
    generateNewCard:function(){

      let indexCard = 0;

      if(card[1]==null){
        indexCard = randomInt(0,25);
      }else{
        switch(randomInt(0,3)){
          case 0 :
            //console.log("Beda");

            var ben = Math.floor(card[1] / 5);
            var idx = 0;
            if(ben > 1 && (ben >= 3 || randomInt(0,2)==0) ){
                idx = randomInt( 0 , ben)*5;
            }else{
                idx = randomInt( ben +1 , 5)*5;
            }

            idx += (randomInt(1,5));

            indexCard = idx;

          break;
          case 1 :
            //console.log("mirip");
            var idx = 0;
            if(randomInt(0,2)){

                idx = (randomInt(0,4)*5) +card[1] % 5;
            }else{

                idx = (randomInt(0,4)) + ((Math.floor(card[1] / 5))*5);

            }
            indexCard = idx;
          break;
          case 2 :
            //console.log("sama");
            indexCard = card[1];
          break;

        }

      }

      let imageNow = this.add.sprite(720 + 720/2 , 1280/2 , 'Car',indexCard);
      imageNow.anchor.setTo(0.5);
      imageNow.scale.setTo(0.5);
      //imageNow.tint = COLOR[randomInt(0 , 3)];

      this.world.bringToTop(item.border);

      var cad = new Card(imageNow);
      cad.move(720/2 , this , false);
      return cad;
    },
    execute:function(key){
      console.log(key);


        if(card[1] == null){
            console.log('pembanding belum ada');
            card[1] = card[0];
            return;
        }

        if(card[0].image.frame % 5 != card[1]%5){
            console.log('INFO EXECUTE : Warna Beda');
            key--;
        }
        if(Math.floor( card[0].image.frame / 5)!= Math.floor(card[1] / 5)){
            console.log('INFO EXECUTE : Gambar Beda');
            key--;
        }
        if(key == 0){
            console.log('JAWABAN : benar');
            let a = this.add.image(720/2 , 877 , 'Info' , 1 );
            a.anchor.setTo(0.5);
            a.scale.setTo(0.7,0.5);
            this.add.tween(a.scale).to( {  y:0.7 }, 200, Phaser.Easing.Sinusoidal.InOut, true);
            var tween =this.add.tween(a.scale).to( {  y:0 }, 300, Phaser.Easing.Sinusoidal.InOut, true , 1000);
            tween.onComplete.add(()=>a.kill(), this);

            gameData.comboTime = 0;
            this.scoreUpdate(100);
            gameData.combo++;
            gameData.correct++;
            if(tutorialMode.isTutorial){
              tutorialMode.text.setText('Yeay Jawaban Kamu Benar');
              gameData.pause();
              item.btn.forEach(function(btn){
                btn.inputEnabled = false;
              }.bind(this));
              this.time.events.add(Phaser.Timer.SECOND * 2, ()=> {item.btnPause.inputEnabled = true;
                tutorialMode.text.setText('Sekarang Coba Klik Tombol\nPause di kiri atas');}, this);
            }

        }else{
            let a = this.add.image(720/2 , 877 , 'Info' , 2 );
            a.anchor.setTo(0.5);
            a.scale.setTo(0.7,0.5);
            this.add.tween(a.scale).to( { y:0.7 }, 200, Phaser.Easing.Sinusoidal.InOut, true);
            var tween =this.add.tween(a.scale).to( { y:0 }, 300, Phaser.Easing.Sinusoidal.InOut, true , 1000);
            tween.onComplete.add(()=>a.kill(), this);

            console.log('JAWABAN : salah');
            gameData.combo = 1;
            gameData.comboTime = 0;
            this.scoreUpdate(-300);
            gameData.false++;
            if(tutorialMode.isTutorial){
              tutorialMode.text.setText('Coba Lagii!\n jangan menyerah');
              gameData.pause();
            }
        }
        card[1] = card[0].image.frame;
    },
    timerUpdate:function(){
          gameData.maxTime--;
          if(gameData.maxTime >120){
            item.hint.text = (gameData.maxTime%120 - 1).toString();
            if(gameData.maxTime != 121)
              return;
          }
          if(gameData.maxTime == 121){
            item.hint.text = "";
            this.flipCard()
            let i = 0;
            item.btn.forEach(function(buton){
                buton.inputEnabled = true;
                buton.info = i;
                buton.events.onInputDown.add(()=> {
                  this.execute(buton.info);
                  this.flipCard();
                 }, this);
                 i++;
            }.bind(this));

            if(tutorialMode.isTutorial){
              tutorialMode.text.setText('Samakan Dengan Mobil\nSebelumnya dan Jawab\ndengan klik tombol dibawah');
              this.time.events.add(Phaser.Timer.SECOND * 10, ()=> {
                gameData.stop();
              }, this);
            }
            //gameData.maxTime = 5;
            return;
          }


          let sec = ':';
          if(gameData.maxTime % 60 < 10){
            sec = ':0'
          }

          item.timeText.setText('0'+(Math.floor(gameData.maxTime/60)).toString()+sec+(gameData.maxTime%60));
          if(gameData.maxTime <= 0){
            gameData.stop();
            this.gameHide(false);
            item.layer.alpha = 0.6;
            this.world.bringToTop(item.layer);
            let a = this.add.button(720/2 , 1280/2 , 'continue_button' ,()=> {this.gameFinish() }, this, 1,0,1);
            a.anchor.setTo(0.5);
            a.scale.setTo(0.7);
          }


    },
    scoreUpdate:function(poin){
        gameData.score += gameData.combo*poin;

        item.scoreText.setText(gameData.score.toString());


    },
    gameHide:function(option){
        let a = 0;
        if(option == true){
            a = 1;
        }


        for(let i = 0 ; i < 4; i++){
          if(i == 0 || i == 4){
            if(card[i] != null){

              card[i].image.alpha = a;
            }
          }else{

            if(card[i]!= null){
              let obj = card[i].first;
              while(obj != null){

                obj.image.alpha = a;
                obj = obj.next;
              }
            }
          }
        }

        item.hint.alpha = a;


        item.btn.forEach(function(obj){
            //obj.alpha = a;
            obj.inputEnabled = option;
        }.bind(this));

    },
    gamePause:function(){
      this.gameHide(this.isPaused);

      this.isPaused = !this.isPaused;
      if(this.isPaused){
          gameData.pause();
          item.layer.alpha = 0.6;
          item.panelPause.alpha =1;
          item.btnMain.inputEnabled = true;
          item.btnMain.alpha = 1;
          item.btnRetry.inputEnabled = !tutorialMode.isTutorial;
          item.btnRetry.alpha = 1;
          item.btnResume.inputEnabled = !tutorialMode.isTutorial;
          item.btnResume.alpha = 1;
          this.world.bringToTop(item.layer);
          this.world.bringToTop(item.panelPause);
          this.world.bringToTop(item.btnRetry);
          this.world.bringToTop(item.btnMain);
          this.world.bringToTop(item.btnResume);

      }else{
          gameData.resume();
          item.layer.alpha = 0;
          item.panelPause.alpha =0;
          item.btnMain.inputEnabled = false;
          item.btnMain.alpha = 0;
          item.btnRetry.inputEnabled = false;
          item.btnRetry.alpha = 0;
          item.btnResume.inputEnabled = false;
          item.btnResume.alpha = 0;

      }



      if(tutorialMode.isTutorial){
          this.world.bringToTop(tutorialMode.panel);
          this.world.bringToTop(tutorialMode.text);
          tutorialMode.text.setText('Tutorial Telah Selesaii\nKamu Bisa Keluar Dengan\nKlik Tombol Main Menu');
          item.btnPause.inputEnabled = false;
      }


    },
    gameFinish:function(){
        console.log("game finished");

        item.btnPause.inputEnabled = false;

        let a =this.add.image(0,0,'ResultBG');
        a.height = 1280;
        a.width = 720;

        a =this.add.image(720/2,500,'result_panel');
        a.anchor.setTo(0.5);
        a.scale.setTo(0.7);

        a = this.add.text(  720/2,   550 , 'Speed Match' ,{
          font: "60px Arial",
          fill: "#f7f7f7",
          align: "center",
          fontWeight:"bold"
        });
        a.anchor.setTo(0.5);
        a = this.add.text(  720/2 -30 ,   700 , 'Score' ,{
          font: "50px Arial",
          fill: "#f7f7f7",
          align: "right",
        });
        a.anchor.setTo(1 , 0.5);
        a = this.add.text(  720/2 -30,   750 , 'Correct' ,{
          font: "50px Arial",
          fill: "#f7f7f7",
          align: "right",
        });
        a.anchor.setTo(1 , 0.5);
        a = this.add.text(  720/2 -30,   800 , 'Avg Speed' ,{
          font: "50px Arial",
          fill: "#f7f7f7",
          align: "right",
        });
        a.anchor.setTo(1 , 0.5);

        a = this.add.text(  720/2 +30 ,   700 , gameData.score.toString() ,{
          font: "50px Arial",
          fill: "#f7f7f7",
          align: "left",
        });
        a.anchor.setTo(0 , 0.5);


        a = this.add.text(  720/2 +30,   750 , gameData.correct.toString()+' of '+(gameData.correct+gameData.false).toString() ,{
          font: "50px Arial",
          fill: "#f7f7f7",
          align: "left",
        });
        a.anchor.setTo(0 , 0.5);
        let avg = 'No Answer';
        if(gameData.correct+gameData.false != 0){
          avg = ((120)/(gameData.correct+gameData.false)).toString();
          avg = avg.split('.');
          if(avg.length > 1){
            avg = avg[0] +'.'+avg[1].substring(0,2);
          }else{
            avg = avg[0];
          }

        }

        a = this.add.text(  720/2 +30,   800 , avg ,{
          font: "50px Arial",
          fill: "#f7f7f7",
          align: "left",
        });
        a.anchor.setTo(0 , 0.5);

        let exitBtn = this.add.button(720/2  , 1000 , 'continue_button' ,()=> {
          target = 'BeforePlay';
          this.state.start('Loading');}, this, 1,0,1);
        exitBtn.anchor.setTo(0.5,0);
        exitBtn.inputEnabled = true;


        if (gameData.score > localStorage.getItem('5')) {
               localStorage.setItem('5', gameData.score);

               var todayDate = (new Date().toISOString().slice(2,10)).split('-');
               console.log(todayDate);


               var d = new Date().toISOString().slice(11,13);
               d = parseInt(d);
               if(d >= 17){
                   var day = parseInt(todayDate[2]);
                   day++;
                   console.log(day);
                   if(day < 10){
                       todayDate[2] = '0'+day.toString();
                   }else{
                       todayDate[2] = day.toString();
                   }


               }
               localStorage.setItem('5-D', todayDate[2]+'/'+todayDate[1]+'/'+todayDate[0]);

               console.log(todayDate[2]);

               for(var i = 5 ; i > 1; i--){
                   var temp = localStorage.getItem(i.toString());
                   var tempdata = localStorage.getItem((i-1).toString());

                   if(parseInt(temp) > parseInt(tempdata)){
                         var temp2 = localStorage.getItem(i.toString()+'-D');

                         localStorage.setItem(i.toString() , tempdata) ;
                         localStorage.setItem((i-1).toString() , temp);

                         localStorage.setItem(i.toString()+'-D' , localStorage.getItem((i-1).toString()+'-D')) ;
                         localStorage.setItem((i-1).toString()+'-D' , temp2);
                   }else{
                       break;
                   }
               }
       }


    }

}
